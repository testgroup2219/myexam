const connection= require('./connection');
const express= require('express');
const bodyParser= require('body-parser');

var app= express();

app.use(bodyParser.json())

app.get('/movie',(req,res)=>
{
    connection.query('SELECT * FROM movie',(err,rows)=>{
        if(err){
            console.log(err)
        }else{
          //  console.log(rows)
          res.send(rows)
        }
    })
})


app.get('/movie/:id',(req,res)=>{
    connection.query('select * from movie where id=?',[req.params.id],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
            //show fetched data on console 
            console.log(rows)
            //show fetched data on browser
            res.send(rows)
        }
    })
})

app.delete('/movie/:id',(req,res)=>
{
    connection.query('DELETE * FROM movie WHERE id=?',[req.params.id],(err,rows)=>{
        if(err){
            console.log(err)
        }else{
          //  console.log(rows)
          res.send(rows)
        }
    })
})


app.delete('/movie/:id',(req,res)=>
{
    connection.query('DELETE * FROM movie WHERE id=?',[req.params.id],(err,rows)=>{
        if(err){
            console.log(err)
        }else{
          //  console.log(rows)
          res.send(rows)
        }
    })
})



app.listen(3000,()=>console.log('Express running on port 3000'))
